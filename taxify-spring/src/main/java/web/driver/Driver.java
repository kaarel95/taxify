package web.driver;

import lombok.AllArgsConstructor;

/**
 * Created by PC on 10/22/2018.
 */
@AllArgsConstructor
public class Driver {
    private String firstName;
    private String lastName;
}
